import React from 'react';
import NavBar from '../navBar/navBar';

export default class App extends React.Component {

  render() {

    return (
    <div>
      <Navbar history={this.props.history} />
        {this.props.children}
    </div>
    );
  }
}
