import React, { Component } from 'react';
//React Bootstrap Library Imports
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
//React-router library to add routing hook inside of the nav component
import { Link } from 'react-router'
// Logo inline-import, required at render()
import Logo from './logo-main.png';

export default class NavBar extends React.Component{

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    require('./logo-main.png');

    const logoStyle = {
      'maxWidth': '200',
      'margin': '10'
    }
    return (
      <Navbar>
        <Nav pullLeft>
          <Link to="/">
            <MenuItem>
              <img style={logoStyle} src={Logo} responsive />
            </MenuItem>
          </Link>
        </Nav>
        <Nav pullRight>
          <NavDropdown id="menu" className="menu_dropdown"
            title={<span>Menu<i className="fa fa-bars"></i></span>}
          >
            <MenuItem >React-Bootstrap</MenuItem>
            <MenuItem >Material-UI</MenuItem>
            <MenuItem >Semantic-UI</MenuItem>
            <MenuItem >Bulma.io</MenuItem>
            <MenuItem divider />
              <MenuItem >
                <Link to="/StyleGuide">
                  Base Style Guide
                </Link>
              </MenuItem >
              <MenuItem >
                <Link to="/Components">
                  Components
                </Link>
              </MenuItem>
          </NavDropdown>
        </Nav>
      </Navbar>
    );
  }
}
