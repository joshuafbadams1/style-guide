import React from 'react';
import { Router, Route, Link, browserHistory } from 'react-router';

import App from './components/app/app';
import Components from './components/components/components';
import Home from './components/home/home';
import SideMenu from './components/sideMenu/sideMenu';
import StyleGuide from './components/styleGuide/styleGuide';

export default (
      <Route component={App}>
        <Route path='/' component={Home} />
        <Route path='/Components' component={Components} />
        <Route path='/SideMenu' component={SideMenu} />
        <Route path='/StyleGuide' component={StyleGuide} />
      </Route>
    );
