# Single Page Application (start)

### Style && Component guide
 -Check wiki for errors or other randomness 

## DEPENDENCIES
->Package.json

It's best install `npm install -g nodemon` easier start syntax
This will make use of the dev server much easier

Start Server: 
`nodemon server.js`

### Dependencies Main

'espress'
-HTTP connection management

'react'
-base library/layout

'file-loader'

'react-dom'
-virtual dom creation

'style-loader'


'url-loader'

'webpack'
-main bundler and webpack middleware/GULP pass


### Dev Dependencies

'babel'
-Use of linting and presents for babel7 update
-Using react hot-loading for increased development

'eslint'
-Code standards and checking babel & react pass through

'nodemon' 
-Node.js server watch

'webpack-dev-middleware'
-server to front-end server pass through